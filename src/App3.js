import React, { useMemo, useState, memo, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';
import { UPDATE_TITLE_SURVEY, UPDATE_DESCRIPTION_SURVEY, ADD_OPTION_IN_QUESTION, DELETE_OPTION_IN_QUESTION, ADD_QUESTION, GENERATOR_SURVEY, REMOVE_SURVEY, UPDATE_TITLE_QUESTION, UPDATE_TITLE_OPTION } from './actions/survey';
import Input from './components/input';
import './global.css';
// https://drive.google.com/file/d/1UDDA6F0D7S38P-MxeqwIjCPPD4ITSzCR/view
export default function App3() {

  const { surveyInfor, questions, isRenderSurvey } = useSelector(state => state.survey);
  const { isAuthen, userInfo } = useSelector(state => state.authen);
  const dispatch = useDispatch();

  const handleAddOption = (questionId) => () => {
    const newOption = {
      id: uuidv4(),
      title: '',
      isLock: false,
    };
    dispatch({
      type: ADD_OPTION_IN_QUESTION,
      payload: {
        newOption,
        questionId,
      },
    });
  };

  const handleDeleteOption = (questionId, optionId, isDisable) => () => {
    if(isDisable) 
      return false;
    dispatch({
      type: DELETE_OPTION_IN_QUESTION,
      payload: {
        questionId,
        optionId,
      }
    })
  };

  const handleAddQuestion = () => {
    dispatch({
      type: ADD_QUESTION,
    });
    window.scrollTo(0, document.body.scrollHeight);
  };

  // const handleDeleteQuestion = (questionId) => (event) => {
  //   const newQuestions = questions.filter(question => question.id !== questionId);
  //   setQuestions(newQuestions);
  // };

  // const handleChangeStatusOption = (questionId, optionId, lock) => () => {
  //   const copyQuestions = [...questions];
  //   for(let i = 0; i < copyQuestions.length; i++) {
  //     if(copyQuestions[i].id === questionId) {
  //       for(let j = 0; j < copyQuestions[i].options.length; j++) {
  //         if(copyQuestions[i].options[j].id === optionId) {
  //           copyQuestions[i].options[j].isLock = lock;
  //           break;
  //         }
  //       }
  //       break;
  //     }
  //   }
  //   setQuestions(copyQuestions);
  // };

  const renderQuestions = useMemo(() => {
    if (questions?.length) {
      return (
        <div style={{ margin: '16px' }}>
          {
            questions.map((question, indexQuestion) => (
              <div style={{ marginBottom: '32px' }} key={question.id}>
                <div>
                  <span>Câu {indexQuestion + 1}:</span>
                  <Input
                    disabled={isRenderSurvey}
                    value={question.title}
                    placeholder={`Tiêu đề câu hỏi số ${indexQuestion + 1}`}
                    onChange={(event) => handleChangeQuestion(question.id, event)}
                  />
                  <button
                    disabled={isRenderSurvey}
                  // onClick={handleDeleteQuestion(question.id)}
                  >
                    X
                  </button>
                </div>
                <div style={{ marginTop: '12px' }}>
                  {
                    question?.options && (
                      question?.options.map((option, indexOption) => (
                        <div key={option.id} style={{ margin: '16px 0px 16px 24px', }}>
                          <span>Đáp án {indexOption + 1}</span>
                          <input
                            onChange={(event) => handleChangeOption(question.id, option.id, event)}
                            name='title'
                            value={option.title}
                            disabled={option.isLock || isRenderSurvey}
                            style={{ width: '300px' }}
                            placeholder={`Đáp án số ${indexOption + 1} câu hỏi số ${indexQuestion + 1}`}
                          />
                          <button 
                            disabled={isRenderSurvey}
                            onClick={handleDeleteOption(question.id, option.id, option.isLock)}
                          >
                            X
                          </button>
                          <button
                            disabled={isRenderSurvey}
                            // onClick={handleChangeStatusOption(question.id, option.id, true)}
                          >
                            Lock
                          </button>
                          <button
                            disabled={isRenderSurvey}
                            // onClick={handleChangeStatusOption(question.id, option.id, false)}
                          >
                            Unlock
                          </button>
                        </div>
                      ))
                    )
                  }
                  <button 
                    onClick={handleAddOption(question.id)}
                  >
                    Add option
                  </button>
                </div>
              </div>
            ))
          }
        </div>
      )
    }
  }, [questions, isRenderSurvey]);

  const handleSurveyInfo = useCallback((event) => {
    const { value, name } = event.target;
    dispatch({
      type: name === 'title' ? UPDATE_TITLE_SURVEY : UPDATE_DESCRIPTION_SURVEY,
      payload: value,
    });
  }, []);

  const handleChangeQuestion = useCallback((event, questionId) => {
    const { value } = event.target;
    dispatch({
      type: UPDATE_TITLE_QUESTION,
      payload: {
        questionId,
        newValue: value,
      },
    });
  }, []);

  const handleChangeOption = useCallback((event, questionId, optionId) => {
    const { value } = event.target;
    dispatch({
      type: UPDATE_TITLE_OPTION,
      payload: {
        questionId,
        optionId,
        newValue: value,
      },
    });
  }, []);

  // const handleChangeOption = (questionId, optionId, { target }) => {
  //   const { value } = target;
  //   dispatch({
  //     type: UPDATE_TITLE_OPTION,
  //     payload: {
  //       questionId,
  //       optionId,
  //       newValue: value,
  //     },
  //   });
  // };

  const handleGeneratorSurvey = () => {
    dispatch({
      type: GENERATOR_SURVEY,
    })
  };

  const handleRemoveSurvey = () => {
    dispatch({
      type: REMOVE_SURVEY,
    })
  };

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ padding: '24px', flex: 1, }}>
        <div>
          <span>
            Title survey
          </span>
          <Input
            disabled={isRenderSurvey}
            value={surveyInfor.title}
            onChange={handleSurveyInfo}
            placeholder='Title'
            name='title'
          />
        </div>
        <br />
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <span>
            Description survey
          </span>
          <Input
            disable={isRenderSurvey}
            value={surveyInfor.description}
            rows={5}
            isTextarea
            placeholder='Descript'
            onChange={handleSurveyInfo}
            name='description'
          />
        </div>
        {/* {renderQuestions} */}
        <div style={{ margin: '16px' }}>
          {
            questions.map((question, indexQuestion) => (
              <div style={{ marginBottom: '32px' }} key={question.id}>
                <div>
                  <span>Câu {indexQuestion + 1}:</span>
                  <Input
                    disabled={isRenderSurvey}
                    value={question.title}
                    placeholder={`Tiêu đề câu hỏi số ${indexQuestion + 1}`}
                    questionId={question.id}
                    onChange={handleChangeQuestion}
                  />
                  <button
                    disabled={isRenderSurvey}
                  // onClick={handleDeleteQuestion(question.id)}
                  >
                    X
                  </button>
                </div>
                <div style={{ marginTop: '12px' }}>
                  {
                    question?.options && (
                      question?.options.map((option, indexOption) => (
                        <div key={option.id} style={{ margin: '16px 0px 16px 24px', }}>
                          <span>Đáp án {indexOption + 1}</span>
                          <Input
                            disabled={option.isLock || isRenderSurvey}
                            value={option.title}
                            questionId={question.id}
                            optionId={option.id}
                            placeholder={`Đáp án số ${indexOption + 1} câu hỏi số ${indexQuestion + 1}`}
                            onChange={handleChangeOption}
                          />
                          <button 
                            disabled={isRenderSurvey}
                            onClick={handleDeleteOption(question.id, option.id, option.isLock)}
                          >
                            X
                          </button>
                          <button
                            disabled={isRenderSurvey}
                            // onClick={handleChangeStatusOption(question.id, option.id, true)}
                          >
                            Lock
                          </button>
                          <button
                            disabled={isRenderSurvey}
                            // onClick={handleChangeStatusOption(question.id, option.id, false)}
                          >
                            Unlock
                          </button>
                        </div>
                      ))
                    )
                  }
                  <button 
                    onClick={handleAddOption(question.id)}
                  >
                    Add option
                  </button>
                </div>
              </div>
            ))
          }
        </div>
        <br />
        <br />
        <br />
        <button style={{ position: 'fixed', bottom: '8px', right: '8px' }} onClick={handleAddQuestion}>Add question</button>
        <br />
        <br />
        <br />
        <button onClick={handleGeneratorSurvey} style={{ position: 'fixed', bottom: '8px', right: '140px' }}>Create Survey</button>
      </div>
      <div style={{ flex: 1, }}>
        Render Survey when onClick
        <br />
        <br />
        <br />
        {
          isRenderSurvey && (
            <button onClick={handleRemoveSurvey}>Remove render</button>
          )
        }
      </div>
    </div>
  )
}
