import axios from "axios"
import { API_URL } from "../constant"

const config = (isFormData = false) => {
  let contentType = 'application/json';
  if(isFormData) {
    contentType = 'multipart/form-data'
  }
  const headers = {
    headers: {
      'Content-Type': contentType,
    }
  };

  return headers;

}

export const login = (data, responseCb, errorCb, finallyCb) => {
  axios.post(`${API_URL}/auth/login`, data, config())
    .then(responseCb)
    .catch(errorCb)
}
