import axios from "axios"
import { API_URL } from "../constant"

const config = (isFormData = false) => {
  const token = localStorage.getItem('access_token');
  let contentType = 'application/json';
  if(isFormData) {
    contentType = 'multipart/form-data'
  }
  const headers = {
    headers: {
      'Content-Type': contentType,
      'Authorization': `Bearer ${token}`
    }
  };

  return headers;

}

export const getProductsList = (responseCb, errorCb) => {
  axios.get(`${API_URL}/products`, config())
    .then(responseCb)
    .catch(errorCb)
}
