import React from 'react'
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom'
import { LOGOUT } from '../../actions/authentication';
import { getProductsList } from '../../services/product'

export default function Product() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const getProducts = () => {
    getProductsList(
      (res) => {
        console.log(res)
      },
      (err) => {
        console.log(err.response);
      }
    )
  }

  const handleLogout = () => {
    localStorage.clear();
    navigate('/');
    dispatch({ type: LOGOUT })
  };
  
  return (
    <div>
      <button onClick={getProducts}>Get List Products</button>
      <button onClick={handleLogout}>Logout</button>
    </div>
  )
}
