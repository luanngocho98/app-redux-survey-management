import React from 'react'
import { useState } from 'react'
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { LOGIN } from '../../actions/authentication';
import { login } from '../../services/authentication';

export default function LoginPage() {
  const [dataLogin, setDataLogin] = useState({
    email: '',
    password: '',
  });
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const handleLogin = () => {
    login(
      JSON.stringify(dataLogin),
      (res) => {
        const { access_token } = res.data;
        localStorage.setItem('access_token', access_token);
        navigate('/products');
        dispatch({ type: LOGIN })
        // get-me => response (get-me) => dispatch({type: GET_ME, payload: response.data})
      },
      (err) => {
        console.log(err)
      },
    )
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setDataLogin({
      ...dataLogin,
      [name]: value,
    })
  };

  return (
    <div>
      <input onChange={handleChange} name='email' value={dataLogin.email} />
      <br/>
      <br/>
      <input onChange={handleChange} name='password' value={dataLogin.password} />
      <br/>
      <br/>
      <button onClick={handleLogin}>Login</button>
    </div>
  )
}
