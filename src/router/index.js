import React, {  } from 'react'
import { useSelector } from 'react-redux';
import {
  Route,
  Routes,
  Navigate
} from "react-router-dom";
import LoginPage from '../pages/login';
import Product from '../pages/product';

export default function ConfigRouter() {
  const { isAuthen, userInfo } = useSelector(state => state.authen);

  return (
    <Routes>
      <Route path="/" element={isAuthen ? <Navigate to={'/products'} /> : <LoginPage />} />
      <Route path="/products" element={!isAuthen ? <Navigate to={'/'} /> : <Product />} />
    </Routes>
  )
}
