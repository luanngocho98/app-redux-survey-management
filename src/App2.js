import React, { useMemo, useState } from 'react'
import { v4 as uuidv4 } from 'uuid';
import './global.css';

export default function App2() {
  const [isRenderSurvey, setIsRenderSurvey] = useState(false);
  const [surveyInfor, setSurveyInfor] = useState({
    title: '',
    description: '',
  })
  const [questions, setQuestions] = useState([
    {
      // Câu hỏi
      id: uuidv4(),
      title: '',
      options: [
        {
          // Câu trả lời
          id: uuidv4(),
          title: '',
          isLock: false,
        }
      ],
    }
  ]);

  const handleAddOption = (questionId) => () => {
    const copyQuestions = [...questions];
    const newOption = {
      id: uuidv4(),
      title: '',
      isLock: false,
    };
    for(let i = 0; i < copyQuestions.length; i++) {
      if(copyQuestions[i].id === questionId) {
        copyQuestions[i].options.push(newOption);
        break;
      }
    }
    setQuestions(copyQuestions);
  };

  const handleDeleteOption = (questionId, optionId, isDisable) => () => {
    if(isDisable) 
      return false;
    const copyQuestions = [...questions];
    for(let i = 0; i < copyQuestions.length; i++) {
      if(copyQuestions[i].id === questionId) {
        const newOptions = copyQuestions[i].options.filter(option => option.id !== optionId);
        copyQuestions[i].options = newOptions;
        break;
      }
    }
    setQuestions(copyQuestions);
  };

  const handleAddQuestion = () => {
    const newQuestion = {
      id: uuidv4(),
      title: '',
      options: [
        {
          // Câu trả lời
          id: uuidv4(),
          title: '',
          isLock: false,
        }
      ],
    };
    setQuestions([...questions, newQuestion]);
    window.scrollTo(0, document.body.scrollHeight);
  };

  const handleDeleteQuestion = (questionId) => (event) => {
    const newQuestions = questions.filter(question => question.id !== questionId);
    setQuestions(newQuestions);
  };

  const handleChangeStatusOption = (questionId, optionId, lock) => () => {
    const copyQuestions = [...questions];
    for(let i = 0; i < copyQuestions.length; i++) {
      if(copyQuestions[i].id === questionId) {
        for(let j = 0; j < copyQuestions[i].options.length; j++) {
          if(copyQuestions[i].options[j].id === optionId) {
            copyQuestions[i].options[j].isLock = lock;
            break;
          }
        }
        break;
      }
    }
    setQuestions(copyQuestions);
  };

  const renderQuestions = useMemo(() => {
    if(questions?.length) {
      return(
        <div style={{ margin: '16px' }}>
          {
            questions.map((question, indexQuestion) => (
              <div style={{ marginBottom: '32px' }} key={question.id}>
                <div>
                  <span>Câu {indexQuestion + 1}:</span>
                  <input disabled={isRenderSurvey} name='title' value={question.title} onChange={(event) => handleChangeQuestion(question.id, event)} placeholder={`Tiêu đề câu hỏi số ${indexQuestion + 1}`}/>
                  <button disabled={isRenderSurvey} onClick={handleDeleteQuestion(question.id)}>X</button>
                </div>
                <div style={{ marginTop: '12px' }}>
                  {
                    question?.options && (
                      question?.options.map((option, indexOption) => (
                        <div key={option.id} style={{ margin: '16px 0px 16px 24px', }}>
                          <span>Đáp án {indexOption + 1}</span>
                          <input onChange={(event) => handleChangeOption(question.id, option.id, event)} name='title' value={option.title} disabled={option.isLock || isRenderSurvey} style={{ width: '300px' }} placeholder={`Đáp án số ${indexOption + 1} câu hỏi số ${indexQuestion + 1}`}/>
                          <button disabled={isRenderSurvey} onClick={handleDeleteOption(question.id, option.id, option.isLock)}>X</button>
                          <button disabled={isRenderSurvey} onClick={handleChangeStatusOption(question.id, option.id, true)}>Lock</button>
                          <button disabled={isRenderSurvey} onClick={handleChangeStatusOption(question.id, option.id, false)}>Unlock</button>
                        </div>
                      ))
                    )
                  }
                  <button onClick={handleAddOption(question.id)}>Add option</button>
                </div>
              </div>
            ))
          }
        </div>
      )
    }
  }, [questions, isRenderSurvey]);

  const handleSurveyInfo = ({ target }) => {
    const { value, name } = target;
    setSurveyInfor({
      ...surveyInfor,
      [name]: value,
    });
  };

  const handleChangeQuestion = (questionId, { target }) => {
    const { value } = target;
    const copyQuestions = [...questions];
    for(let i = 0; i < copyQuestions.length; i++) {
      if(copyQuestions[i].id === questionId) {
        copyQuestions[i].title = value;
        break;
      }
    }
    setQuestions(copyQuestions);
  };

  const handleChangeOption = (questionId, optionId, { target }) => {
    const { value } = target;
    const copyQuestions = [...questions];
    for(let i = 0; i < copyQuestions.length; i++) {
      if(copyQuestions[i].id === questionId) {
        for(let j = 0; j < copyQuestions[i].options.length; j++) {
          if(copyQuestions[i].options[j].id === optionId) {
            copyQuestions[i].options[j].title = value;
            break;
          }
        }
        break;
      }
    }
    setQuestions(copyQuestions);
  };

  const handleGeneratorSurvey = () => {
    setIsRenderSurvey(true);
  };

  const handleRemoveSurvey = () => {
    setIsRenderSurvey(false);
  };

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ padding: '24px', flex: 1, }}>
        <div>
          <span>
            Title survey
          </span> 
          <input disabled={isRenderSurvey} name='title' value={surveyInfor.title} onChange={handleSurveyInfo} placeholder='Title' />
        </div>
        <br/>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <span>
            Description survey
          </span> 
          <textarea disabled={isRenderSurvey} name='description' value={surveyInfor.description} onChange={handleSurveyInfo} rows={5} placeholder='Descript' />
        </div>
        {renderQuestions}
        <br/>
        <br/>
        <br/>
        <button style={{ position: 'fixed', bottom: '8px', right: '8px' }} onClick={handleAddQuestion}>Add question</button>
        <br/>
        <br/>
        <br/>
        <button onClick={handleGeneratorSurvey} style={{ position: 'fixed', bottom: '8px', right: '140px' }}>Create Survey</button>
      </div>
      <div style={{ flex: 1, }}>
        Render Survery when onClick
        <br/>
        <br/>
        <br/>
        {
          isRenderSurvey && (
            <button onClick={handleRemoveSurvey}>Remove render</button>
          )
        }
      </div>
    </div>
  )
}
