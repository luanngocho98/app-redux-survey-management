import React, { Fragment, useMemo, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

const DELETE_ICON_URL = 'https://cdn2.iconfinder.com/data/icons/flat-ui-icons-24-px/24/cross-24-512.png';

export default function App() {
  const [files, setFiles] = useState([]);

  const handleSelectFile = (event) => {
    const { files: filesInput } = event.target; // rename files to filesInput
    const file = filesInput[0];
    const imageUrl = URL.createObjectURL(file)
    const imageItem = {
      id: uuidv4(),
      imageUrl,
      file: file,
    }
    setFiles([...files, imageItem]);
    // delete image
    event.target.value = '';
  };

  const handleDelete = (fileId) => () => {
    const newFiles = files.filter(file => file.id !== fileId);
    setFiles(newFiles);
    // remove file in DOM
    const file = files.filter(file => file.id === fileId)[0].file;
    URL.revokeObjectURL(file);
  };

  const renderFiles = useMemo(() => {
    if(files?.length) {
      return(
        <div
          style={{
            display: 'flex',
          }}
        >
          {
            files.map(file => (
              <div key={file.id} style={{ margin: '16px 8px', position: 'relative' }} onClick={handleDelete(file.id)}>
                  <img
                    alt='Image item'
                    src={file.imageUrl}
                    width="250px"
                    height="250px"
                    style={{ objectFit: 'contain', background: 'grey',
                    }}
                  />
                  <img
                    alt='Icon'
                    src={DELETE_ICON_URL}
                    width="25px"
                    height="25px"
                    style={{ position: 'absolute', right: '15px', top: '15px', cursor: 'pointer' }}
                  />
                </div>
            ))
          }
        </div>
      )
    } else {
      return ''
    }
    
  }, [files]);

  return (
    <Fragment>
      <div>
        <input
          type='file'
          onChange={handleSelectFile}
        />
      </div>
      {renderFiles}
    </Fragment>
  )
}
