import React, { Fragment, memo } from 'react'

function Input(props) {
  const { isDisable, value, placeholder, onChange, questionId = null, optionId = null, isTextarea, row, name = '' } = props;

  console.log('re-render');

  const handleChange = (event) => {
    onChange(event, questionId, optionId)
  }

  return (
    <Fragment>
      {
        isTextarea ? (
          <textarea
            disabled={isDisable}
            row={row}
            value={value}
            onChange={handleChange}
            placeholder={placeholder}
            name={name}
          />
        ) : (
          <input
            style={{ width: optionId && '300px' }}
            disabled={isDisable}
            value={value}
            onChange={handleChange}
            placeholder={placeholder}
            name={name}
          />
        )
      }
    </Fragment>
    
  )
}

export default memo(Input); //high order component => HOC, cơ chế so sánh ===
