import React, {  } from 'react'
import {
  BrowserRouter,
} from "react-router-dom";
import ConfigRouter from './router';

export default function Main() {
  return (
    <BrowserRouter>
      <ConfigRouter />
    </BrowserRouter>
  )
}
