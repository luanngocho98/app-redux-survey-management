import { combineReducers } from "redux";
import authenReducer from "./authentication";
import surveyReducer from "./survey";

const rootReducer = combineReducers({
  survey: surveyReducer,
  authen: authenReducer,
})
export default rootReducer;
