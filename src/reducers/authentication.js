import { v4 as uuidv4 } from 'uuid';
import { LOGIN, LOGOUT } from '../actions/authentication';

const initialState = {
  isAuthen: !!localStorage.getItem('access_token'),
  userInfo: null, // { id: 1, fullName: '', avatar: '', phoneNumber: '', }
}
const authenReducer = (state = initialState, action) => {
  // action: {type: constant, payload: {}}
  // state: initialState {}
  switch (action.type) {
    case LOGIN: {
      return {
        ...state,
        isAuthen: true,
      }
    }
    case LOGOUT: {
      return {
        ...state,
        isAuthen: false,
      }
    }
    default:
      return state;
  }
};
export default authenReducer;
