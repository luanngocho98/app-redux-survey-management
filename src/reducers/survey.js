import { v4 as uuidv4 } from 'uuid';
import { ADD_OPTION_IN_QUESTION, ADD_QUESTION, DELETE_OPTION_IN_QUESTION, GENERATOR_SURVEY, REMOVE_SURVEY, UPDATE_DESCRIPTION_SURVEY, UPDATE_TITLE_OPTION, UPDATE_TITLE_QUESTION, UPDATE_TITLE_SURVEY } from '../actions/survey';
const initialState = {
  surveyInfor: {
    title: '',
    description: '',
  },
  questions: [
    {
      id: uuidv4(),
      title: '',
      options: [
        {
          id: uuidv4(),
          title: '',
        }
      ]
    }
  ],
  isRenderSurvey: false,
}
const surveyReducer = (state = initialState, action) => {
  // action: {type: constant, payload: {}}
  // state: initialState {}
  switch (action.type) {
    case UPDATE_TITLE_SURVEY: {
      return {
        ...state,
        surveyInfor: {
          ...state.surveyInfor,
          title: action.payload,
        }
      }
    }
    case UPDATE_DESCRIPTION_SURVEY: {
      return {
        ...state,
        surveyInfor: {
          ...state.surveyInfor,
          description: action.payload,
        }
      }
    }
    case ADD_OPTION_IN_QUESTION: {
      const copyQuestions = [...state.questions];
      for(let i = 0; i < copyQuestions.length; i++) {
        if(copyQuestions[i].id === action.payload.questionId) {
          copyQuestions[i].options.push(action.payload.newOption);
          break;
        }
      }
      return {
        ...state,
        questions: copyQuestions,
      }
    }
    case DELETE_OPTION_IN_QUESTION: {
      const copyQuestions = [...state.questions];
      for(let i = 0; i < copyQuestions.length; i++) {
        if(copyQuestions[i].id === action.payload.questionId) {
          const newOptions = copyQuestions[i].options.filter(option => option.id !== action.payload.optionId);
          copyQuestions[i].options = newOptions;
          break;
        }
      }
      return {
        ...state,
        questions: copyQuestions,
      }
    }
    case ADD_QUESTION: {
      const newQuestion = {
        id: uuidv4(),
        title: '',
        options: [
          {
            // Câu trả lời
            id: uuidv4(),
            title: '',
            isLock: false,
          }
        ],
      };
      return {
        ...state,
        questions: [...state.questions, newQuestion],
      }
    }
    case UPDATE_TITLE_QUESTION: {
      const copyQuestions = [...state.questions];
      for(let i = 0; i < copyQuestions.length; i++) {
        if(copyQuestions[i].id === action.payload.questionId) {
          copyQuestions[i].title = action.payload.newValue;
          break;
        }
      }
      return {
        ...state,
        questions: copyQuestions,
      }
    }
    case UPDATE_TITLE_OPTION: {
      const copyQuestions = [...state.questions];
      for(let i = 0; i < copyQuestions.length; i++) {
        if(copyQuestions[i].id === action.payload.questionId) {
          for(let j = 0; j < copyQuestions[i].options.length; j++) {
            if(copyQuestions[i].options[j].id === action.payload.optionId) {
              copyQuestions[i].options[j].title = action.payload.newValue;
              break;
            }
          }
          break;
        }
      }
      return {
        ...state,
        questions: copyQuestions,
      }
    }
    case GENERATOR_SURVEY: {
      return {
        ...state,
        isRenderSurvey: true,
      }
    }
    case REMOVE_SURVEY: {
      return {
        ...state,
        isRenderSurvey: false,
      }
    }
    default:
      return state;
  }
};
export default surveyReducer;
